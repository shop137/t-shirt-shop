FROM python:3.9-alpine
RUN mkdir /app
WORKDIR /app
COPY requirements.txt /app
COPY source /app/source
COPY uwsgi.ini /etc/uwsgi/uwsgi.ini
COPY entrypoint.sh /entrypoint.sh
RUN apk update \
    && apk add postgresql gcc python3-dev musl-dev libffi-dev
RUN pip install -r requirements.txt
RUN chown -R nobody:nogroup /app
RUN chmod +x /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]
CMD ["uwsgi", "--ini", "/etc/uwsgi/uwsgi.ini"]
