from django.db.models import Q
from rest_framework.response import Response

from api_v1.models import Product


def get_with_filter_products(data):
    data = data.get("main_filters", {})
    filters = []
    if data:
        if data.get("category_id"):
            filters.append(Q(categoryproduct__category__in=data.get("category_id")))
        if data.get("hot_sale"):
            filters.append(Q(hot=True))
        if data.get("new_sale"):
            filters.append(Q(new=True))
        res = Product.objects.filter(*filters)
        return res

    return Product.objects.all()

