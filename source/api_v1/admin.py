from django.contrib import admin

from api_v1.models import Product, CategoryProduct, ProductAttribute, FilterOption, Category, ProductImage

admin.site.register(Product)
admin.site.register(ProductImage)
admin.site.register(Category)
admin.site.register(FilterOption)
admin.site.register(CategoryProduct)
admin.site.register(ProductAttribute)
