from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from api_v1.models import Product, Category
from api_v1.serializers import ProductSerializer, ProductRetrieveSerializer, CategorySerializer, CategoryListSerializer
from api_v1.service import get_with_filter_products


class ProductsView(ModelViewSet):
    def create(self, request, *args, **kwargs):
        data = request.data
        res = get_with_filter_products(data)
        data = ProductSerializer(res, many=True).data
        return Response(data=data, status=200)


class ProductsRetriveView(ModelViewSet):

    def retrieve(self, request, *args, **kwargs):
        id = kwargs.get("id", None)
        if not id:
            return Response(data={"detail": "id is required field"}, status=404)
        try:
            product = Product.objects.get(id=id)
        except:
            return Response(data={"detail": "FAIL"}, status=404)
        data = ProductRetrieveSerializer(product).data
        return Response(data=data, status=200)


class CategoryProductView(ModelViewSet):
    def retrieve(self, request, *args, **kwargs):
        id = kwargs.get("id", None)
        if not id:
            return Response(data={"detail": "id is required field"}, status=404)
        try:
            category = Category.objects.get(id=id)
        except:
            return Response(data={"detail": "FAIL"}, status=404)

        data = CategorySerializer(category).data
        return Response(data=data, status=200)


class CategoriesView(ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategoryListSerializer

