from rest_framework.serializers import Serializer, ModelSerializer

from api_v1.models import Product, Category, CategoryProduct


class ProductSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = ["id", "title", "description", "price", "hot", "new", "created_at"]

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["category"] = instance.categoryproduct_set.values_list("category_id")
        data["images"] = instance.productimage_set.values("image", "type")
        return data


class ProductRetrieveSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = ["id", "title", "description", "price", "hot", "new", "created_at"]

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["category"] = instance.categoryproduct_set.values_list("category_id")
        data["type"] = instance.productattribute_set.values("type", "option__value")
        data["images"] = instance.productimage_set.values("image", "type")
        return data


class CategoryProductSerializer(ModelSerializer):
    product = ProductRetrieveSerializer()

    class Meta:
        model = CategoryProduct
        fields = ["product"]


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ["id", "title", "photo"]

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["products"] = CategoryProductSerializer(instance.categoryproduct_set.all(), many=True).data
        return data


class CategoryListSerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ["id", "title", "photo"]