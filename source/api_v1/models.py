from django.db import models


class Product(models.Model):
    title = models.CharField(max_length=255, verbose_name="Заголовок")
    description = models.CharField(max_length=1024, verbose_name="Описание")
    price = models.IntegerField(default=0, verbose_name="Цена")
    hot = models.BooleanField(verbose_name="Хит продаж", default=False)
    new = models.BooleanField(verbose_name="Новейшие", default=False)
    created_at = models.DateTimeField(auto_now_add=True)


class ProductImage(models.Model):
    MAIN_IMAGE = "main"
    PRE_IMAGE = "pre"

    PRODUCT_CHOICE = (
        (MAIN_IMAGE, "Главная картина"),
        (PRE_IMAGE, "Картина")
    )

    product = models.ForeignKey("Product", on_delete=models.CASCADE)
    image = models.ImageField(upload_to='product', verbose_name="Изображение")
    type = models.CharField(max_length=255, choices=PRODUCT_CHOICE)


class Category(models.Model):
    title = models.CharField(max_length=255, verbose_name="Название категории")
    photo = models.ImageField(upload_to="category")


class CategoryProduct(models.Model):
    product = models.ForeignKey("Product", on_delete=models.CASCADE)
    category = models.ForeignKey("Category", on_delete=models.SET_NULL, null=True, blank=True)


class FilterOption(models.Model):
    value = models.CharField(max_length=255, verbose_name="Значение")


class ProductAttribute(models.Model):
    TYPES = "type"
    COLOR = "color"

    ATTRIBUTE_CHOICE = (
        (TYPES, "Тип"),
        (COLOR, "Цвет")
    )
    product = models.ForeignKey("Product", on_delete=models.CASCADE)
    option = models.ForeignKey("FilterOption", on_delete=models.CASCADE)
    type = models.CharField(max_length=255, choices=ATTRIBUTE_CHOICE)


